import 'package:flutter/material.dart';
import 'dart:math';

class GamePage extends StatefulWidget {
  const GamePage({super.key, required this.title});

  final String title;

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  int _gamePoint = 10000;
  int _countWin = 0;
  int _countDraw = 0;
  int _countLose = 0;
  String _gameMessage = "";

  int? _myChoice = null;
  int? _comChoice = null;

  final List<Map> _gameItem = [
    { 'name' : '가위', 'imgSrc': 'assets/scissors.jpg' },
    { 'name' : '가위', 'imgSrc': 'assets/rock.jpg' },
    { 'name' : '가위', 'imgSrc': 'assets/paper.jpg' },
  ];

  void _gameStart(int itemIndex) {
    setState(() {
      if (_gamePoint < 1000) {
        _gameMessage = "포인트가 부족해요! Reset 해요!";
      } else {
        _myChoice = itemIndex;
        _comChoice = Random().nextInt(_gameItem.length);

        if ((_myChoice == 0 && _comChoice == 1) || (_myChoice == 1 && _comChoice == 2)) {
          _countLose += 1;
          _gamePoint -= 1000;
          _gameMessage = "졌어요! 1000점을 잃었어요!";
        } else if ((_myChoice == 0 && _comChoice == 2) || (_myChoice == 1 && _comChoice == 0)) {
          _countWin += 1;
          _gamePoint += 1000;
          _gameMessage = "이겼어요! 1000점을 얻었어요!";
        } else {
          _countDraw += 1;
          _gameMessage = "비겼어요! 아무것도 없어요!";
        }
      }
    });
  }

  void _gameReset() {
    setState(() {
      _gamePoint = 10000;
      _countWin = 0;
      _countDraw = 0;
      _countLose = 0;
      _gameMessage = "";

      _myChoice = null;
      _comChoice = null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(
          children: [
            Text("Point : $_gamePoint"),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text("이김 : $_countWin"),
                Text("비김 : $_countDraw"),
                Text("짐 : $_countLose"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                OutlinedButton(
                    onPressed: () {
                      _gameStart(0);
                    },
                    child: Text("가위")
                ),
                OutlinedButton(
                    onPressed: () {
                      _gameStart(1);
                    },
                    child: Text("바위")
                ),
                OutlinedButton(
                    onPressed: () {
                      _gameStart(2);
                    },
                    child: Text("보")
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text("나"),
                    Image.asset(
                      "${_myChoice == null ? 'assets/all.jpg' : _gameItem[_myChoice!]['imgSrc']}",
                      width: 50,
                      height: 50,
                    )
                  ],
                ),
                Text("VS"),
                Column(
                  children: [
                    Text("COM"),
                    Image.asset(
                      "${_comChoice == null ? 'assets/all.jpg': _gameItem[_comChoice!]['imgSrc']}",
                      width: 50,
                      height: 50,
                    ),
                  ],
                ),
              ],
            ),
            Text(_gameMessage),
            OutlinedButton(
                onPressed: () {
                  _gameReset();
            }, child: Text("Reset")),
          ],
        ),
    );
  }
}
